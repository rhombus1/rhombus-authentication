package com.rhombus.domain.controller;

import com.rhombus.api.dto.authentication.CredentialResetDto;
import com.rhombus.api.dto.authentication.CustomerCheckDto;
import com.rhombus.api.dto.authentication.LoginDto;
import com.rhombus.api.dto.authentication.RegistrationDto;
import com.rhombus.domain.dal.entity.Credential;
import com.rhombus.domain.enums.ResponseCode;
import com.rhombus.domain.service.AuthenticationService;
import com.rhombus.mapper.DtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 26/9/2020
 * Time: 10:10
 */

@Controller
public class AuthenticationControllerImpl implements AuthenticationController {

	private final AuthenticationService authenticationService;
	private final DtoMapper             mapper;

	@Autowired
	public AuthenticationControllerImpl(AuthenticationService authenticationService, DtoMapper mapper) {

		this.authenticationService = authenticationService;
		this.mapper = mapper;
	}

	@Override
	public ResponseEntity<CredentialResetDto> changePassword(CredentialResetDto credentialResetDto) {

		return ResponseEntity.ok(mapper.map(authenticationService.changePassword(mapper.map(credentialResetDto))));
	}

	@Override
	public ResponseEntity<CustomerCheckDto> checkCustomer(String username, String clientId) {

		Credential credential = authenticationService.getCredentialByUsernameAndClientId(username, clientId);

		CustomerCheckDto customerCheck = new CustomerCheckDto();
		customerCheck.setUsername(username);
		customerCheck.setResponseCode(ResponseCode.APPROVED);
		customerCheck.setResponseDescription("APPROVED");

		if (credential == null) {
			customerCheck.setRegistered(false);
		} else {
			customerCheck.setRegistered(true);
			customerCheck.setPendingPassword(credential.getPassword() == null && credential.getPasswordResetToken() != null);
		}
		return ResponseEntity.ok(customerCheck);
	}

	@Override
	public ResponseEntity<LoginDto> login(LoginDto loginDto) {

		return ResponseEntity.ok(mapper.map(authenticationService.login(mapper.map(loginDto))));
	}

	@Override
	public ResponseEntity<RegistrationDto> register(RegistrationDto registrationDto) {

		return ResponseEntity.ok(mapper.map(authenticationService.register(mapper.map(registrationDto))));
	}

	@Override
	public ResponseEntity<Boolean> requestForgotPassword(String username, String clientId) {

		return ResponseEntity.ok(authenticationService.requestForgotPassword(username, clientId));
	}

	@Override
	public ResponseEntity<CredentialResetDto> resetPassword(CredentialResetDto credentialResetDto) {

		return ResponseEntity.ok(mapper.map(authenticationService.resetPassword(mapper.map(credentialResetDto))));
	}
}
