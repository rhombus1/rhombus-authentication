package com.rhombus.domain.controller;

import com.rhombus.api.dto.authentication.CredentialResetDto;
import com.rhombus.api.dto.authentication.CustomerCheckDto;
import com.rhombus.api.dto.authentication.LoginDto;
import com.rhombus.api.dto.authentication.RegistrationDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 26/9/2020
 * Time: 10:09
 */

@RestController
@RequestMapping(value = "authentication",
		consumes = "application/json",
		produces = "application/json")
public interface AuthenticationController {

	@RequestMapping("/`changePassword")
	@PostMapping()
	ResponseEntity<CredentialResetDto> changePassword(@RequestBody CredentialResetDto credentialResetDto);

	@RequestMapping("/customer/check")
	@GetMapping()
	ResponseEntity<CustomerCheckDto> checkCustomer(@RequestParam String username, @RequestParam String clientId);

	@RequestMapping("/login")
	@PostMapping()
	ResponseEntity<LoginDto> login(@RequestBody LoginDto loginDto);

	@RequestMapping("/register")
	@PostMapping()
	ResponseEntity<RegistrationDto> register(@RequestBody RegistrationDto registrationDto);

	@RequestMapping("/forgotPassword")
	@PostMapping()
	ResponseEntity<Boolean> requestForgotPassword(@RequestParam String username, @RequestParam String clientId);

	@RequestMapping("/resetPassword")
	@PostMapping()
	ResponseEntity<CredentialResetDto> resetPassword(@RequestBody CredentialResetDto credentialResetDto);
}
