package com.rhombus.domain.service;

import com.rhombus.domain.dal.entity.Credential;
import com.rhombus.domain.service.model.CredentialReset;
import com.rhombus.domain.service.model.Login;
import com.rhombus.domain.service.model.Registration;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 5/10/2020
 * Time: 11:48
 */

public interface AuthenticationService {

	CredentialReset changePassword(CredentialReset credentialReset);

	Credential getCredentialByUsernameAndClientId(String username, String clientId);

	Login login(Login login);

	Registration register(Registration registration);

	Boolean requestForgotPassword(String username, String clientId);

	CredentialReset resetPassword(CredentialReset credentialReset);
}
