package com.rhombus.domain.service;

import com.rhombus.domain.dal.entity.ContactDetail;
import com.rhombus.domain.dal.entity.Credential;
import com.rhombus.domain.dal.entity.User;
import com.rhombus.domain.enums.ResponseCode;
import com.rhombus.domain.service.model.CredentialReset;
import com.rhombus.domain.service.model.Login;
import com.rhombus.domain.service.model.Registration;
import com.rhombus.exception.EntityNotFoundException;
import com.rhombus.utils.CryptoUtils;
import com.rhombus.utils.random.RandomGenerateStrategy;
import com.rhombus.utils.random.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 5/10/2020
 * Time: 11:49
 */

@Component
public class AuthenticationServiceImpl implements AuthenticationService {

	private final CredentialService credentialService;
	private final UserService       userService;

	@Value("${server.encryption.key:0123456789ABCDEFFEDCBA9876543210}")
	private String encryptionKey;

	@Autowired
	public AuthenticationServiceImpl(CredentialService credentialService, UserService userService) {

		this.credentialService = credentialService;
		this.userService = userService;
	}

	@Override
	public CredentialReset changePassword(CredentialReset credentialReset) {

		Credential credential = credentialService.findCredentialByUsername(credentialReset.getUsername());

		if (credential == null || !credential.getClientId().equals(credentialReset.getClientId())) {
			credentialReset.setResponseCode(ResponseCode.NOT_FOUND);
			credentialReset.setResponseDescription("Credential for username " + credentialReset.getUsername() + " " +
					"and clientId " + credentialReset.getClientId() + " was not found");
			return credentialReset;
		}

		if (isValidateCredential(credentialReset, credential)) {
			String clearPassword = CryptoUtils.decrypt(encryptionKey, credentialReset.getPassword());
			String savedClearPassword = CryptoUtils.decrypt(encryptionKey, credential.getPassword());
			if (clearPassword == null || !clearPassword.equals(savedClearPassword)) {
				credentialReset.setResponseCode(ResponseCode.INVALID_CREDENTIAL);
				credentialReset.setResponseDescription("Invalid Credentials");
				return credentialReset;
			}
			String newPassword = CryptoUtils.decrypt(encryptionKey, credentialReset.getNewEncryptedPassword());
			credential.setPassword(CryptoUtils.encrypt(encryptionKey, newPassword));
			credentialService.put(credential);

			credentialReset.setResponseCode(ResponseCode.APPROVED);
			credentialReset.setResponseDescription("APPROVED");
		}
		return credentialReset;
	}

	@Override
	public Credential getCredentialByUsernameAndClientId(String username, String clientId) {

		return credentialService.findCredentialByUsername(username);
	}

	private boolean isValidateCredential(CredentialReset credentialReset, Credential credential) {

		if (credential.isLocked()) {
			credentialReset.setResponseCode(ResponseCode.USER_LOCKED);
			credentialReset.setResponseDescription("Credential Locked");
			return false;
		}

		if (credential.isExceedsAttempts()) {
			credentialReset.setResponseCode(ResponseCode.ATTEMPTS_EXCEEDED);
			credentialReset.setResponseDescription("Attempts exceeded");
			return false;
		}
		return true;
	}

	@Override
	public Login login(Login login) {

		if (login == null) {
			login = new Login();
			login.setResponseCode(ResponseCode.ERROR);
			login.setResponseDescription("Login object cannot be null");
			return login;
		}
		Credential credential = credentialService.findCredentialByUsername(login.getUsername());

		if (credential == null || !credential.getClientId().equals(login.getClientId())) {
			login.setResponseCode(ResponseCode.NOT_FOUND);
			login.setResponseDescription("Credential for username " + login.getUsername() + " and clientId " + login.getClientId() +
					" was not found");
			return login;
		}

		if (credential.isLocked()) {
			login.setResponseCode(ResponseCode.INVALID_CREDENTIAL);
			login.setResponseDescription("Credential Locked");
			return login;
		}

		if (credential.isExceedsAttempts()) {
			login.setResponseCode(ResponseCode.INVALID_CREDENTIAL);
			login.setResponseDescription("Attempts exceeded");
			return login;
		}

		// validate password
		String savedClearPassword = CryptoUtils.decrypt(encryptionKey, credential.getPassword());
		String clearPassword = CryptoUtils.decrypt(encryptionKey, login.getEncryptedPassword());

		if (savedClearPassword == null || clearPassword == null) {
			login.setResponseCode(ResponseCode.INVALID_CREDENTIAL);
			login.setResponseDescription("Invalid Credentials");
			return login;
		}
		if (savedClearPassword.equals(clearPassword)) {
			login.setUser(credential.getUser());
			login.setResponseCode(ResponseCode.APPROVED);
			login.setResponseDescription("APPROVED");
		} else {
			login.setResponseCode(ResponseCode.INVALID_CREDENTIAL);
			login.setResponseDescription("Invalid Credentials");
		}

		return login;
	}

	@Override
	public Registration register(Registration registration) {

		if (credentialService.findCredentialByUsername(registration.getUsername()) != null) {
			registration.setResponseCode(ResponseCode.DUPLICATE);
			registration.setResponseDescription("User with username " + registration.getUsername() + " already " +
					"exists");
		}
		User user = new User();
		user.setFirstName(registration.getFirstName());
		user.setLastName(registration.getLastName());
		user.setNotificationToken((String) registration.getAdditionalData().get("NOTIFICATION_TOKEN"));

		ContactDetail contactDetail = new ContactDetail();
		contactDetail.setCity(registration.getCity());
		contactDetail.setCountry(registration.getCountry());
		contactDetail.setEmail(registration.getEmail());
		contactDetail.setLocality(registration.getLocality());
		contactDetail.setTelephone(registration.getTelephone());
		user.setContactDetail(contactDetail);
		user = userService.post(user);

		Credential credential = new Credential();
		credential.setActive(true);
		credential.setAttempts(0);
		credential.setClientId(registration.getClientId());
		credential.setExceedsAttempts(false);
		credential.setLocked(false);
		credential.setPassword(registration.getEncryptedPassword());
		credential.setUser(user);
		credential.setUsername(registration.getUsername());

		credentialService.post(credential);

		registration.setResponseCode(ResponseCode.APPROVED);
		registration.setResponseDescription("APPROVED");
		return registration;
	}

	@Override
	public Boolean requestForgotPassword(String username, String clientId) {

		String passwordResetToken =
				RandomUtil.generateRandomPassword(8, RandomGenerateStrategy.ALPHANUMERIC_UPPER);
		Credential credential = credentialService.findCredentialByUsername(username);

		if (credential == null) {
			throw new EntityNotFoundException("Credential for username not found: " + username);
		}
		credential.setPasswordResetToken(passwordResetToken);
		credential.setPassword(null);
		credentialService.put(credential);

		return true;
	}

	@Override
	public CredentialReset resetPassword(CredentialReset credentialReset) {

		Credential credential = credentialService.findCredentialByUsername(credentialReset.getUsername());

		if (credential == null || !credential.getClientId().equals(credentialReset.getClientId())) {
			credentialReset.setResponseCode(ResponseCode.NOT_FOUND);
			credentialReset.setResponseDescription("Credential for username " + credentialReset.getUsername() + " " +
					"and" + " clientId " + credentialReset.getClientId() + " was not found");
			return credentialReset;
		}

		if (isValidateCredential(credentialReset, credential)) {
			if (credential.getPasswordResetToken() == null || credential.getPasswordResetToken().isEmpty()) {
				credentialReset.setResponseCode(ResponseCode.INVALID_TOKEN);
				credentialReset.setResponseDescription("Credential token is invalid");
				return credentialReset;
			}

			if (!credential.getPasswordResetToken().equals(credentialReset.getPasswordResetToken())) {
				credentialReset.setResponseCode(ResponseCode.INVALID_TOKEN);
				credentialReset.setResponseDescription("Credential token is invalid");
				return credentialReset;
			}
			credential.setPasswordResetToken(null);

			String clearPassword = CryptoUtils.decrypt(encryptionKey, credentialReset.getPassword());
			credential.setPassword(CryptoUtils.encrypt(encryptionKey, clearPassword));
			credentialService.put(credential);

			credentialReset.setResponseCode(ResponseCode.APPROVED);
			credentialReset.setResponseDescription("APPROVED");
		}
		return credentialReset;
	}
}
